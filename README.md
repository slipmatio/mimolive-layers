# WIP: Slipmat.io mimoLive Layers

A collection of free open source templates for mimoLive designed for Slipmat.io.

Note: this repo is still very much a work in progress.

## Layers

### Value From Web

Reads an attribute from JSON file and displays it as text. The given URL is polled once every 10s to refresh the value.

## Related Documentation

- https://docs.mimo.live/docs/custom-layers